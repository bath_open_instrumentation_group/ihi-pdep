FROM python:3.7-alpine

# Make our /deploy directory
RUN mkdir -p /deploy
COPY app.py config.py boot.sh requirements.txt /deploy/

# Make our main app directory
RUN mkdir -p /deploy/website
COPY website/ /deploy/website/

# Make our instance directory
RUN mkdir -p /deploy/instance/data

# Make our db migrations directory
RUN mkdir -p /deploy/migrations
COPY migrations/ /deploy/migrations/

# Install requirements
RUN pip install -r /deploy/requirements.txt
RUN pip install gunicorn

# Make boot executable
RUN chmod +x /deploy/boot.sh

# Set up environment for flask/gunicorn
ENV FLASK_APP "/deploy/app.py"
ENV FLASK_ENV "development"
ENV FLASK_RUN_PORT 5000

# Set up environment variables that should be changed
ENV SECRET_KEY "changeme"
ENV ADMIN_PASSWORD "changeme"

WORKDIR /deploy

# Expose port 5000
EXPOSE 5000

# Set entrypoint to boot.sh
ENTRYPOINT ["/deploy/boot.sh"]
