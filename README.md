# IHI-PDEP

## To make a new release

### Before creating a release

To guarantee proper database migration paths, ensure that each release includes an alembic migration file where appropriate (i.e. if the database models have changed).

* `flask db init` (may fail if database already exists, this is fine though)
* `flask db migrate` (may fail if database is unchanged, this is fine though)

To apply the migration to your local database, run:

* `flask db upgrade`

This commands are called whenever the docker container is started, ensuring that the database is properly synchronised with the models defined in this application.
### Creating a release

* Create a tag in GitLab
  * `Repository` `Tags`, `New Tag`
  * Set tag name to the release version number
* The CI will automatically build the Docker image and deploy it to the GitLab registry

## To use the Docker image

Suggested to use docker-compose, with a `docker-compose.yaml` like the following:

```
version: '3'
services:
    ihi-pdep:
        image: registry.gitlab.com/bath_open_instrumentation_group/ihi-pdep:latest
        container_name: ihi-pdep
        ports:
            - 5000:5000
        volumes:
            - '/data/ihi-pdep:/deploy/instance/data'
        environment:
            - SECRET_KEY: 'my_secret_key'
```

Replace `'my_secret_key'` with some long, secret string.

