import os
import sys
import logging

from website.app import create_app
from website.firstrun import setup

from website.models import db
from flask_migrate import Migrate

from config import CONFIG

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

app = create_app(CONFIG)
migrate = Migrate(app, db)


@app.cli.command()
def initdb():
    logging.info("Creating tables...")
    db.create_all()
    with app.app_context():
        setup()


@app.cli.command()
def dropdb():
    db.drop_all()
