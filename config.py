import os
import logging
import string
import random

def gen_key(length: int):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

# Database URI
project_dir = os.path.dirname(os.path.abspath(__file__))

data_dir = os.path.join(project_dir, "instance", "data")
if not os.path.exists(data_dir):
    os.makedirs(data_dir)

database_uri = "sqlite:///{}".format(os.path.join(data_dir, "db.sqlite"))

# Secret key from environment variable
secret_key = os.environ.get("SECRET_KEY", default=None)
if not secret_key:
    logging.warning("No SECRET_KEY environment variable found. A key will be generated but sessions will not persist after application restarts.")
    secret_key = gen_key(32)

CONFIG = {
    "SQLALCHEMY_DATABASE_URI": database_uri,
    "SQLALCHEMY_TRACK_MODIFICATIONS": False,
    "SECRET_KEY": secret_key,
}
