"""empty message

Revision ID: ca444ba28937
Revises: 720491a319fc
Create Date: 2019-08-28 10:19:07.350384

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ca444ba28937"
down_revision = "720491a319fc"
branch_labels = None
depends_on = None


def upgrade():
    # Add new data
    op.add_column("patient", sa.Column("bad_slide", sa.Integer(), nullable=True))
    op.add_column(
        "patient", sa.Column("taking_medication", sa.Integer(), nullable=True)
    )

    # Add new column for MRDT results
    op.add_column("patient", sa.Column("provisional_mrdt", sa.Integer(), nullable=True))

    # Rename final MRDT section to final diagnosis
    with op.batch_alter_table("patient") as batch_op:
        batch_op.alter_column("final_mrdt", new_column_name="final_malaria")


def downgrade():
    op.drop_column("patient", "taking_medication")
    op.drop_column("patient", "bad_slide")

    op.drop_column("patient", "provisional_mrdt")

    with op.batch_alter_table("patient") as batch_op:
        batch_op.alter_column("final_malaria", new_column_name="final_mrdt")
