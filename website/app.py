import os
import datetime
import logging
from flask import Flask

from .models import db
from .methods import current_user

from . import routes


def create_app(config=None):
    app = Flask(__name__, instance_relative_config=True)

    # Inject some variables to all routes
    logging.info("Injecting variables into all web routes...")

    @app.context_processor
    def inject_globals():
        today_string = datetime.date.today().strftime("%Y-%m-%d")
        user = current_user()
        return dict(today=today_string, user=user)

    # Load public config from dictionary
    logging.info("Loading Flask app configuration...")
    if config and isinstance(config, dict):
        app.config.from_mapping(config)

    # Load secret config from file
    app.config.from_pyfile("config.py", silent=True)

    # Ensure the instance folder exists
    logging.info("Checking /instance directory...")
    try:
        logging.info("Creating /instance directory...")
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Register blueprints
    logging.info("Registering blueprints...")
    app.register_blueprint(routes.base, url_prefix="")
    app.register_blueprint(routes.patient, url_prefix="/patient")
    app.register_blueprint(routes.auth, url_prefix="/auth")

    # Initialise database
    logging.info("Attaching database to app...")
    db.init_app(app)

    return app
