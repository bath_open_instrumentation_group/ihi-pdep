import os
import logging

from .models import db, User
from .methods import new_user


def setup():

    logging.info("Checking if admin account exists...")
    admin_check = User.query.filter_by(username="admin").first()

    if not admin_check:
        logging.info("No existing administrator account found.")
        logging.info("Fetching target admin password from environment variable...")
        admin_pass = os.environ.get("ADMIN_PASSWORD", default=None)

        if admin_pass:
            logging.info("Valid admin password found.")
            logging.info("Creating new administrator account...")
            admin_obj = new_user("admin", admin_pass, fullname="Administrator")
            admin_obj.is_admin = True
            admin_obj.is_inviter = True
            db.session.add(admin_obj)
            db.session.commit()
        else:
            logging.warn(
                "No ADMIN_PASSWORD environment variable found. Unable to create admin account."
            )

    else:
        logging.info("Existing administrator account found. Leaving unchanged.")
