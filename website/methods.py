from .models import Patient, User, symptom_map, badslide_map
from .utilities import hasher, fuzzy_bool
from flask import session, url_for, abort, redirect
from functools import update_wrapper
import secrets
import datetime


def requires_admin(f):
    def wrapper(*args, **kwargs):
        user = current_user()

        if user and user.is_admin:
            return f(*args, **kwargs)
        else:
            return abort(403)

    return update_wrapper(wrapper, f)


def requires_login(f):
    def wrapper(*args, **kwargs):
        user = current_user()

        if user:
            return f(*args, **kwargs)
        else:
            return redirect(url_for("website.routes.auth.login"))

    return update_wrapper(wrapper, f)


def session_is_op(patient):
    user = current_user()

    if (patient and patient.user_id_created == user.id) or user.is_admin:
        return True
    else:
        return False


def new_user(username, password, fullname=""):
    salt = secrets.token_hex(16)
    hashword = hasher(password, salt)

    return User(username=username, salt=salt, hashword=hashword, fullname=fullname)


def current_user():
    if "id" in session:
        uid = session["id"]
        return User.query.get(uid)
    return None


def fill_missing(form_data):
    for key, symptom in symptom_map.items():
        if not key in form_data:
            form_data[key] = 0
    return form_data


def get_symptoms_from_form(form_data):
    d = {}
    for key in symptom_map.keys():
        # Get radiobox value, default to 0
        d[key] = form_data.get(key, 0)
        # Build the key we use to store details
        details_key = f"{key}_details"
        # Get details value, default to empty string
        d[details_key] = form_data.get(details_key, "")

    return d


def get_badslide_reasons_from_form(form_data):
    d = {}
    for key in badslide_map.keys():
        # Get raw form data
        value = form_data.get(key, False)
        # Get bool values, default to null
        d[key] = fuzzy_bool(value)

    return d


def new_patient(form_data):
    patient = Patient(
        # Patient DB entry information
        patient_id=form_data["patient_id"],
        # Modification and creation
        user_created=current_user(),
        # Patient information
        birth_month=int(form_data["birth_month"]),
        birth_year=int(form_data["birth_year"]),
        date=datetime.datetime.strptime(form_data["date"], "%Y-%m-%d"),
        date_onset=datetime.datetime.strptime(form_data["date_onset"], "%Y-%m-%d"),
        physician=form_data.get("physician", ""),
        facility=form_data.get("facility", ""),
        # Diagnosis information
        provisional_diagnosis=form_data.get("provisional_diagnosis", ""),
        provisional_malaria=form_data.get("provisional_malaria"),
        provisional_mrdt=form_data.get("provisional_mrdt"),
        final_diagnosis=form_data.get("final_diagnosis", ""),
        final_malaria=form_data.get("final_malaria"),
        taking_medication=form_data.get("taking_medication"),
        # Slide quality
        bad_slide=form_data.get("bad_slide"),
        bad_slide_notes=form_data.get("bad_slide_notes", ""),
        **get_badslide_reasons_from_form(form_data),
        # Symptoms
        **get_symptoms_from_form(form_data),
    )

    return patient


def update_patient(patient, form_data):
    form_data = fill_missing(form_data)

    # Modification and creation
    patient.user_modified = current_user()

    # Patient information
    patient.birth_month = int(form_data["birth_month"])
    patient.birth_year = int(form_data["birth_year"])
    patient.date = datetime.datetime.strptime(form_data["date"], "%Y-%m-%d")
    patient.date_onset = datetime.datetime.strptime(form_data["date_onset"], "%Y-%m-%d")

    patient.physician = form_data.get("physician", "")
    patient.facility = form_data.get("facility", "")

    # Diagnosis information
    patient.provisional_diagnosis = form_data.get("provisional_diagnosis", "")
    patient.provisional_malaria = form_data.get("provisional_malaria")
    patient.provisional_mrdt = form_data.get("provisional_mrdt")

    patient.final_diagnosis = form_data.get("final_diagnosis", "")
    patient.final_malaria = form_data.get("final_malaria")

    patient.taking_medication = form_data.get("taking_medication")

    # Slide quality
    patient.bad_slide = form_data.get("bad_slide")
    patient.bad_slide_notes = form_data.get("bad_slide_notes", "")

    for key, val in get_badslide_reasons_from_form(form_data).items():
        if hasattr(patient, key):
            setattr(patient, key, val)

    # Symptoms
    for key, val in get_symptoms_from_form(form_data).items():
        if hasattr(patient, key):
            setattr(patient, key, val)
