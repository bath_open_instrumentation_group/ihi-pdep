from flask_sqlalchemy import SQLAlchemy
import datetime
from .utilities import hasher

db = SQLAlchemy()

short_str_len = 64
detail_str_len = 128
long_str_len = 512

symptom_map = {
    "fever": "Fever",
    "chills": "Chills",
    "headache": "Headache",
    "muscle_pain": "Muscle Pain",
    "diziness": "Diziness",
    "malaise": "Malaise",
    "no_appetite": "No appetite",
    "vomiting": "Vomiting",
    "stomach_ache": "Stomach ache",
    "convulsion": "Convulsion",
}

badslide_map = {
    "not_cleaned": "Slide not cleaned before smear preparation",
    "not_dry": "Smear not dry before staining",
    "not_filtered": "Giemsa stain not well filtered before use",
    "poorly_washed": "Slides poorly washed after staining",
    "over_10pct": "More than 10% of Giemsa used",
    "overstained": "Slide overstained",
    "thin_not_fixed": "Thin smear not fixed",
    "thick_fixed": "Thick smear fixed",
}


class Patient(db.Model):
    __tablename__ = "patient"

    # Database ID (not the same as patient ID)
    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)

    # Patient DB entry information
    patient_id = db.Column(db.String(short_str_len), unique=True, nullable=False)
    deleted = db.Column(db.Boolean, default=False)
    verified = db.Column(db.Boolean, default=False)

    # Modification and creation
    user_id_created = db.Column(db.Integer, db.ForeignKey("user.id"))
    user_id_modified = db.Column(db.Integer, db.ForeignKey("user.id"))
    user_id_verified = db.Column(db.Integer, db.ForeignKey("user.id"))

    user_created = db.relationship("User", foreign_keys=[user_id_created])
    user_modified = db.relationship("User", foreign_keys=[user_id_modified])
    user_verified = db.relationship("User", foreign_keys=[user_id_verified])

    date_created = db.Column(db.DateTime(timezone=True), default=datetime.datetime.now)
    date_modified = db.Column(
        db.DateTime(timezone=True), onupdate=datetime.datetime.now
    )

    # Patient information
    birth_month = db.Column(db.Integer, nullable=True)
    birth_year = db.Column(db.Integer, nullable=True)
    date = db.Column(db.Date)
    date_onset = db.Column(db.Date)

    physician = db.Column(db.String(short_str_len))
    facility = db.Column(db.String(short_str_len))

    # Diagnosis information

    # Integer truth values
    # 0: No data given
    # 1: Determined yes
    # 2: Determined no
    # 3: Determined unclear

    provisional_diagnosis = db.Column(db.String(long_str_len))
    provisional_malaria = db.Column(db.Integer, default=0)
    provisional_mrdt = db.Column(db.Integer, default=0, nullable=True)

    final_diagnosis = db.Column(db.String(long_str_len))
    final_malaria = db.Column(db.Integer, default=0)

    # Slide quality
    bad_slide = db.Column(db.Integer, default=0, nullable=True)  # Is the slide damaged?
    bad_slide_notes = db.Column(db.String(long_str_len))  # How is the slide damaged?

    not_cleaned = db.Column(db.Boolean, nullable=True)
    not_dry = db.Column(db.Boolean, nullable=True)
    not_filtered = db.Column(db.Boolean, nullable=True)
    poorly_washed = db.Column(db.Boolean, nullable=True)
    over_10pct = db.Column(db.Boolean, nullable=True)
    overstained = db.Column(db.Boolean, nullable=True)
    thin_not_fixed = db.Column(db.Boolean, nullable=True)
    thick_fixed = db.Column(db.Boolean, nullable=True)

    taking_medication = db.Column(
        db.Integer, default=0, nullable=True
    )  # Is the patient taking medication AT THE TIME OF CAPTURE?

    # Symptoms
    fever = db.Column(db.Integer, default=0)
    fever_details = db.Column(db.String(detail_str_len), default="")
    chills = db.Column(db.Integer, default=0)
    chills_details = db.Column(db.String(detail_str_len), default="")
    headache = db.Column(db.Integer, default=0)
    headache_details = db.Column(db.String(detail_str_len), default="")
    muscle_pain = db.Column(db.Integer, default=0)
    muscle_pain_details = db.Column(db.String(detail_str_len), default="")
    diziness = db.Column(db.Integer, default=0)
    diziness_details = db.Column(db.String(detail_str_len), default="")
    malaise = db.Column(db.Integer, default=0)
    malaise_details = db.Column(db.String(detail_str_len), default="")
    no_appetite = db.Column(db.Integer, default=0)
    no_appetite_details = db.Column(db.String(detail_str_len), default="")
    vomiting = db.Column(db.Integer, default=0)
    vomiting_details = db.Column(db.String(detail_str_len), default="")
    stomach_ache = db.Column(db.Integer, default=0)
    stomach_ache_details = db.Column(db.String(detail_str_len), default="")
    convulsion = db.Column(db.Integer, default=0)
    convulsion_details = db.Column(db.String(detail_str_len), default="")

    def __repr__(self):
        return "<ID: {}>".format(self.id)


class User(db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), unique=True)

    fullname = db.Column(db.String(short_str_len))

    is_admin = db.Column(db.Boolean, default=False)
    is_inviter = db.Column(db.Boolean, default=True)

    salt = db.Column(db.String(32), unique=True)
    hashword = db.Column(db.String(128))

    def __str__(self):
        return self.username

    def get_user_id(self):
        return self.id

    def check_password(self, password):
        return hasher(password, self.salt) == self.hashword

    def make_hashword(self, password):
        return hasher(password, self.salt)

    @property
    def json(self):
        response = {"username": self.username, "id": self.id, "is_admin": self.is_admin}
        return response
