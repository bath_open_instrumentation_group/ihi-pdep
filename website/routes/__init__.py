from .base import blueprint as base
from .patient import blueprint as patient
from .auth import blueprint as auth
