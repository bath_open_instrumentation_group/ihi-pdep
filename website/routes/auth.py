from flask import Blueprint, request, session, url_for
from flask import render_template, redirect, abort

from ..methods import current_user, new_user, requires_admin, requires_login
from ..models import db, User

blueprint = Blueprint(__name__, "auth")


@blueprint.route("/login", methods=(["GET", "POST"]))
def login():
    err = []

    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        user = User.query.filter_by(username=username).first()

        # If incorrect username or mismatching password
        if not user or (user and not user.check_password(password)):
            err.append("Incorrect username or password.")
        else:
            session["id"] = user.id

    user = current_user()

    return render_template("login.html", user=user, err=err)


@blueprint.route("/logout")
@requires_login
def logout():
    del session["id"]
    return redirect("/")


@blueprint.route("/admin")
@requires_admin
def admin():
    err = []
    user = current_user()

    user_table = User.query.all()

    return render_template("admin.html", user=user, user_table=user_table, err=err)


@blueprint.route("/admin/makeadmin", methods=["POST"])
@requires_admin
def make_admin():
    user_id = request.form.get("user_id")
    state = bool(int(request.form.get("state")))

    user = User.query.filter_by(id=user_id).first()

    if not user:
        return abort(404)

    user.is_admin = state

    db.session.commit()
    return redirect(url_for("website.routes.auth.admin"))


@blueprint.route("/admin/makeinviter", methods=["POST"])
@requires_admin
def make_inviter():
    user_id = request.form.get("user_id")
    state = bool(int(request.form.get("state")))

    user = User.query.filter_by(id=user_id).first()

    if not user:
        return abort(404)

    user.is_inviter = state

    db.session.commit()
    return redirect(url_for("website.routes.auth.admin"))


@blueprint.route("/reset/<user_id>", methods=["GET", "POST"])
@requires_admin
def reset(user_id):
    err = []
    target = User.query.filter_by(id=user_id).first()

    if not target:
        err.append("User does not exist")
        return abort(404)

    if request.method == "POST":
        password = request.form.get("password")
        password_repeat = request.form.get("password_repeat")

        # Check for mismatched passwords
        if password != password_repeat:
            err.append("Passwords do not match!")

        if not err:
            target.hashword = target.make_hashword(password)

        db.session.commit()
        return redirect(url_for("website.routes.auth.admin"))
    else:
        return render_template("reset.html", target=target, err=err)


@blueprint.route("/register", methods=("GET", "POST"))
# @requires_admin
def register():
    user = current_user()

    err = []

    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        password_repeat = request.form.get("password_repeat")
        fullname = request.form.get("fullname")

        # Check for mismatched passwords
        if password != password_repeat:
            err.append("Passwords do not match!")

        # Check for existing username
        user_check = User.query.filter_by(username=username).first()
        if user_check:
            err.append("Username already taken.")

        if not err:
            new_user_obj = new_user(username, password, fullname=fullname)
            db.session.add(new_user_obj)
            db.session.commit()

            # If not an admin, send back to homepage once registered
            return redirect(url_for(".admin"))

    # If admin, or error in registration, stay on the registration page
    return render_template("register.html", user=user, err=err)
