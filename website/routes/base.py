from flask import Blueprint, request
from flask import render_template

from ..models import Patient
from ..methods import current_user, requires_login

blueprint = Blueprint(__name__, "base")


@blueprint.route("/", methods=["GET"])
@requires_login
def home():

    user = current_user()

    search_query = request.args.get("search")

    if search_query:
        patient_query = Patient.query.filter_by(patient_id=search_query)
    else:
        patient_query = Patient.query.filter_by(deleted=False)

    # Remove patients not associated with the current user
    if not user.is_admin:
        patient_query = patient_query.filter_by(user_id_created=user.id)

    patients = [patient.__dict__ for patient in patient_query]
    patients.reverse()

    return render_template("home.html", patients=patients, search_query=search_query)
