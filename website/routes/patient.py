from flask import Blueprint, request, session, url_for
from flask import render_template, redirect, abort

from ..models import db, Patient, symptom_map, badslide_map
from ..methods import (
    new_patient,
    update_patient,
    requires_login,
    requires_admin,
    session_is_op,
    current_user,
)

blueprint = Blueprint(__name__, "patient")


@blueprint.route("/new", methods=["POST"])
@requires_login
def new():
    err = []

    patient_id = request.form.get("patient_id")
    return redirect(url_for("website.routes.patient.create", patient_id=patient_id))


@blueprint.route("/create/<patient_id>", methods=["GET", "POST"])
@requires_login
def create(patient_id):
    err = []

    patient = Patient.query.filter_by(patient_id=patient_id).first()

    if patient:
        patient_dict = patient.__dict__
        return render_template("basicinfo.html", patient=patient_dict)

    if request.form:
        form_data = request.form.to_dict()
        print(form_data)
        if not ("patient_id" in form_data and form_data["patient_id"]):
            err.append("Missing required field patient_id")
        else:
            try:
                patient = new_patient(form_data)
                db.session.add(patient)
                db.session.commit()
            except Exception as e:
                print("Failed to add patient")
                print(e)
                err.append(e)
            else:
                return redirect(url_for("website.routes.base.home"))

    return render_template(
        "form.html",
        patient=None,
        new_patient_id=patient_id or "",
        symptom_map=symptom_map,
        badslide_map=badslide_map,
        err=err,
        mode="w",
    )


@blueprint.route("/create/", methods=["GET", "POST"])
@requires_login
def create_new():
    return create(None)


@blueprint.route("/read/<patient_id>", methods=["GET"])
@requires_login
def read(patient_id):
    err = []

    # Retrieve patient dictionary, if the patient exists
    patient = Patient.query.filter_by(patient_id=patient_id).first()
    if not patient:
        return abort(404)

    # If the current user is not the original poster or an admin
    if not session_is_op(patient):
        return abort(403)

    patient_dict = patient.__dict__

    return render_template(
        "form.html",
        patient=patient_dict,
        symptom_map=symptom_map,
        badslide_map=badslide_map,
        err=err,
        mode="r",
    )


@blueprint.route("/update/<patient_id>", methods=["GET", "POST"])
@requires_login
def update(patient_id):
    err = []

    # Retrieve patient dictionary, if the patient exists
    patient = Patient.query.filter_by(patient_id=patient_id).first()
    if not patient:
        return abort(404)

    # If the current user is not the original poster or an admin
    if not session_is_op(patient):
        return abort(403)

    # If form data is included, and the patient exists
    if request.form and patient:
        form_data = request.form.to_dict()
        try:
            update_patient(patient, form_data)
            db.session.commit()
            patient = Patient.query.filter_by(patient_id=patient_id).first()
        except Exception as e:
            print("Failed to update patient")
            print(e)
            err.append(e)
        else:
            return redirect(url_for("website.routes.base.home"))

    if patient:
        patient_dict = patient.__dict__
    else:
        patient_dict = None
        err.append("No patient found with id {}".format(patient_id))

    return render_template(
        "form.html",
        patient=patient_dict,
        symptom_map=symptom_map,
        badslide_map=badslide_map,
        err=err,
        mode="w",
    )


@blueprint.route("/delete", methods=["POST"])
@requires_login
def delete():
    patient_id = request.form.get("patient_id")
    patient = Patient.query.filter_by(patient_id=patient_id).first()
    if not patient:
        return abort(404)

    # If the current user is not the original poster or an admin
    if not session_is_op(patient):
        return abort(403)

    # If entry has not been verified
    if not patient.verified:
        # Delete the entry from the database
        db.session.delete(patient)
    # If the entry has been verified, and the current user is an admin
    elif current_user().is_admin:
        # Flag the entry as deleted (without deleteing from db)
        patient.deleted = True

    db.session.commit()
    return redirect("/")


@blueprint.route("/verify", methods=["POST"])
@requires_admin
def verify():
    patient_id = request.form.get("patient_id")
    patient = Patient.query.filter_by(patient_id=patient_id).first()
    if not patient:
        return abort(404)

    # If entry has not been verified
    if not patient.verified:
        patient.verified = True

    db.session.commit()
    return redirect("/")
