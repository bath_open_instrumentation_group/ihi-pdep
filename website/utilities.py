import hashlib


def hasher(password, salt):
    return hashlib.sha512((password + salt).encode()).hexdigest()


def fuzzy_bool(value):
    if value in ["on", "On", "1", 1, "true", "True", "yes", "Yes"]:
        return True
    else:
        return False
